#!/usr/bin/env python

import os, sys
import http.client
import json
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom


mode = "xml"
version = "2.5.1"
baseurl = "https://hl7-definition.caristix.com/v2-api/1/HL7v"
connection = http.client.HTTPSConnection("hl7-definition.caristix.com", timeout=2)
headers = {
    "Content-type": "application/xml", # "application/json",
    "referer": f"https://hl7-definition.caristix.com/v2/HL7v{version}/Segments"
}

def get(uri: str):
    uri = f"/v2-api/1/HL7v{version}/{uri}"
    h = headers

    sys.stdout.write(uri + " ")
    connection.request('GET', uri, headers=h)
    response = connection.getresponse()
    content = response.read().decode('utf-8')
    print(response.status)
    
    return content, response


def downloadType(typeName):
    ext = "json"
    if mode != "json":
        ext = "xml"
    os.makedirs(f"xml/{version}/{typeName}", exist_ok=True)
    data, res = get(typeName)
    file = f"xml/{version}/{typeName}.{ext}"
    if res.status == 200:
        if mode != "json":
            # write xml, pretty printed
            data = minidom.parseString(data).toprettyxml(indent="\t")
        fp = open(file, "w")
        fp.write(data)
        fp.close()
    else:
        return None

    if mode == "json":
        parseJson(data, typeName)
    else:
        parseXml(data, typeName)

def parseJson(data: str, typeName: str):
    # loop over all tables
    seg = json.loads(data)
    for item in seg:
        # fetch al lsegments
        id = item["id"]
        f = f"xml/{version}/{typeName}/{id}.json"
        if (os.path.isfile(f)):
            continue

        data, res = get(f"{typeName}/{id}")
        if res.status != 200:
            sys.stderr.write("Error: failed to fetch " + id)
            continue
        fp = open(f, "w")
        fp.write(data)
        fp.close()

def parseXml(data: str, typeName: str):
    # get namespaces
    file = f"xml/{version}/{typeName}.xml"
    namespaces = dict([node for _, node in ET.iterparse(file, events=['start-ns'])])
    default_ns = namespaces['']

    # parse xml
    root = ET.fromstring(data)
    ids_tags = root.findall(f".//{{{default_ns}}}Id")
    for e in ids_tags:
        id = e.text
        f = f"xml/{version}/{typeName}/{id}.xml"
        if (os.path.isfile(f)):
            continue

        data, res = get(f"{typeName}/{id}")
        if res.status != 200:
            sys.stderr.write("Error: failed to fetch " + id)
            continue
        
        data = minidom.parseString(data).toprettyxml(indent="\t")
        fp = open(f, "w")
        fp.write(data)
        fp.close()
    

def main(argv: list):
    global version


    v = [ "2.1", "2.2", "2.3", "2.3.1", "2.4", "2.5", "2.5.1", "2.6", "2.7", "2.7.1", "2.8" ]
    #v = [ "2.5.1" ]
    for cv in v:
        version = cv
        downloadType("DataTypes")
        downloadType("Tables")
        downloadType("TriggerEvents")
        downloadType("Segments")

if __name__ == "__main__":
    main(sys.argv)