CREATE TABLE Version (
    "pk" INTEGER PRIMARY KEY AUTOINCREMENT,
    "id" INTEGER, -- PRIMARY KEY AUTOINCREMENT,
    "version" TEXT
);

CREATE TABLE "DataType" (
    "pk" INTEGER PRIMARY KEY AUTOINCREMENT,
    "id" TEXT,
    "name" TEXT,
    "description" TEXT,
    "length" INTEGER,
    "childCount" INTEGER,
    "version" TEXT,

    -- deduplication pointers
    name_ptr INTEGER,
    description_ptr INTEGER
);

CREATE TABLE "DataTypeComponent" (
    "pk" INTEGER PRIMARY KEY AUTOINCREMENT,
    "id" TEXT, -- Type.name
    "pos" INTEGER,
    "type" TEXT,
    "name" TEXT,
    "repeat" TEXT,
    "length" INTEGER,
    "description" TEXT,
    "table" TEXT, -- table id
    "usage" TEXT CHECK( usage IN ('O', 'R', 'B', 'C', 'W', 'RE') ),
    "version" TEXT,

    -- deduplication pointers
    name_ptr INTEGER,
    description_ptr INTEGER
);

CREATE TABLE "Segment" (
    "pk" INTEGER PRIMARY KEY AUTOINCREMENT,
    "id" TEXT,
    "name" TEXT,
    "description" TEXT,
    "chapter" TEXT,
    "childCount" INTEGER,
    "version" TEXT,

    -- deduplication pointers
    name_ptr INTEGER,
    description_ptr INTEGER
);

CREATE TABLE "Field" (
    "pk" INTEGER PRIMARY KEY AUTOINCREMENT,
    "id" TEXT, -- Type.name
    "pos" INTEGER,
    "type" TEXT,
    "name" TEXT,
    "description" TEXT,
    "repeat" TEXT,
    "length" INTEGER,
    "table" TEXT, -- table id
    "usage" TEXT CHECK( usage IN ('O', 'R', 'B', 'C', 'W', 'X') ),
    "version" TEXT,

    -- deduplication pointers
    name_ptr INTEGER
    --description_ptr INTEGER
);

CREATE TABLE "FieldUsage" (
    id TEXT,
    name TEXT,
    enum_name TEXT
);
INSERT INTO FieldUsage (id, name, enum_name) VALUES ('O', 'Optional', 'OPTIONAL');
INSERT INTO FieldUsage (id, name, enum_name) VALUES ('R', 'Required', 'REQUIRED');
INSERT INTO FieldUsage (id, name, enum_name) VALUES ('B', 'Backward Compatibility', 'COMPATIBILITY');
INSERT INTO FieldUsage (id, name, enum_name) VALUES ('C', 'Conditional', 'CONDITIONAL');
INSERT INTO FieldUsage (id, name, enum_name) VALUES ('W', 'Withdrawn', 'WITHDRAWN');
INSERT INTO FieldUsage (id, name, enum_name) VALUES ('X', 'Not Supported', 'NOT_SUPPORTED');
INSERT INTO FieldUsage (id, name, enum_name) VALUES ('RE', 'Required but may be empty', 'REQUIRED_EMPTY');

CREATE TABLE "Tables" (
    "pk" INTEGER PRIMARY KEY AUTOINCREMENT,
    "id" TEXT, -- Type.name
    "name" TEXT,
    "description" TEXT,
    "chapter" TEXT,
    "type" TEXT,
    "childCount" INTEGER,
    "version" TEXT,

    -- deduplication pointers
    name_ptr INTEGER,
    description_ptr INTEGER
);

CREATE TABLE "TableEntries" (
    "pk" INTEGER PRIMARY KEY AUTOINCREMENT,
    "id" TEXT, -- Type.name
    "value" TEXT,
    "description" TEXT,
    "version" TEXT,

    -- deduplication pointers
    --name_ptr INTEGER,
    description_ptr INTEGER
);

CREATE TABLE "Strings" (
    "pk" INTEGER PRIMARY KEY AUTOINCREMENT,
    "string" TEXT
);
