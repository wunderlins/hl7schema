#!/usr/bin/env bash

VERSION=2.5.1
baseurl="https://hl7-definition.caristix.com/v2-api/1/HL7v$VERSION"
#segments="$baseurl/Segments"

if [[ "x$1" == "x" ]]; then
    echo "Usage: $0 <URI> [jq-filter]"
    exit 1;
fi

filter='.'
if [[ "x$2" != "x" ]]; then
    filter=$2
fi

wget --header="referer: https://hl7-definition.caristix.com/v2/HL7v2.5.1/Segments" \
     --header="accept: application/json" \
     -q "$baseurl/$1" -O- | \
    jq $filter