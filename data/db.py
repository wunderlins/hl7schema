import sqlite3

dbfile = "data/schema2.db"

_con = None
_cur = None

class DbConnException(Exception):
    pass

def escape(str: str) -> str:
    if str == None: return 'NULL'
    return "'" + str.replace("'", "''") + "'"

def dbConnection(dbFile=None):
    global _con, _cur

    if _con == None and dbFile == None:
        raise DbConnException()

    if _con != None:
        return _con, _cur
    
    _con = sqlite3.connect(dbFile)
    _cur = _con.cursor()

    return _con, _cur