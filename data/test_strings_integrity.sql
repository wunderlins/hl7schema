-- check for empty joins
select dt.pk, UPPER(TRIM(dt.name)) ut, dt.name, s.string
FROM DataType dt LEFT JOIN Strings s on dt.name_ptr = s.pk
WHERE -- UPPER(TRIM(dt.name)) != UPPER(TRIM(s.string))
s.string IS NULL
;

-- check for empty name refs
select *
FROM  DataType
WHERE name IS NOT NULL AND name_ptr IS NULL
;