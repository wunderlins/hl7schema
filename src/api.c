#include "api.h"

hl7type_handle *get_version_by_str(const char *vstr) {
    hl7type_handle *v = version;
    while (v != NULL) {
        if (strcmp(v->version, vstr) == 0)
            return v;
        v = v->next;
    }
    return NULL;
}

hl7type_handle *get_version(int vint) {
    hl7type_handle *v = version;
    while (v != NULL) {
        if (v->id == vint)
            return v;
        v = v->next;
    }
    return NULL;
}

Table *get_table(const char *id, int version) {
    int i = 0;

    hl7type_handle *v = get_version(version);
    if (v == NULL)
        return NULL;
    
    TableList *list = v->get_table();
    for (i=0; i< list->length; i++) {
        Table *tbl = (Table *) list->items+i;
        if (strcmp(tbl->id, id) == 0)
            return tbl;
    }

    return NULL;
}

DataType *get_type(const char *id, int version) {
    int i = 0;

    hl7type_handle *v = get_version(version);
    if (v == NULL)
        return NULL;
    
    DataTypeList *list = v->get_types();
    for (i=0; i< list->length; i++) {
        DataType *typ = (DataType *) list->items+i;
        if (strcmp(typ->id, id) == 0)
            return typ;
    }

    return NULL;
}

Segment *get_segment(const char *id, int version) {
    int i = 0;

    hl7type_handle *v = get_version(version);
    if (v == NULL)
        return NULL;
    
    SegmentList *list = v->get_seg();
    for (i=0; i< list->length; i++) {
        Segment *seg = (Segment *) list->items+i;
        if (strcmp(seg->id, id) == 0)
            return seg;
    }

    return NULL;
}