#ifndef _SEGMENTS_280_H_
#define _SEGMENTS_280_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_280();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_280_H_
