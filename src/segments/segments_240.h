#ifndef _SEGMENTS_240_H_
#define _SEGMENTS_240_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_240();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_240_H_
