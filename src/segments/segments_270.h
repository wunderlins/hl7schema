#ifndef _SEGMENTS_270_H_
#define _SEGMENTS_270_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_270();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_270_H_
