#ifndef _SEGMENTS_271_H_
#define _SEGMENTS_271_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_271();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_271_H_
