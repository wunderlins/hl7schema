#ifndef _SEGMENTS_260_H_
#define _SEGMENTS_260_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_260();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_260_H_
