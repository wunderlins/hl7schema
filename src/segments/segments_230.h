#ifndef _SEGMENTS_230_H_
#define _SEGMENTS_230_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_230();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_230_H_
