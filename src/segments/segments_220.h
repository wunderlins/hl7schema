#ifndef _SEGMENTS_220_H_
#define _SEGMENTS_220_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_220();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_220_H_
