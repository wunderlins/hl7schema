#ifndef _SEGMENTS_250_H_
#define _SEGMENTS_250_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_250();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_250_H_
