#ifndef _SEGMENTS_251_H_
#define _SEGMENTS_251_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_251();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_251_H_
