#ifndef _SEGMENTS_231_H_
#define _SEGMENTS_231_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_231();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_231_H_
