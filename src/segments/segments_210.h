#ifndef _SEGMENTS_210_H_
#define _SEGMENTS_210_H_
#ifdef __cplusplus
extern "C" {
#endif

SegmentList *HL7SCHNS_get_seg_210();
#ifdef __cplusplus
}
#endif

#endif // _SEGMENTS_210_H_
