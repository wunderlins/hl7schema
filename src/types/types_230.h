#ifndef _TYPES_230_H_
#define _TYPES_230_H_
#ifdef __cplusplus
extern "C" {
#endif

extern DataTypeComponent *typ_AD_230_entries;
extern DataType *typ_AD_230;
extern DataTypeComponent *typ_CD_230_entries;
extern DataType *typ_CD_230;
extern DataTypeComponent *typ_CE_230_entries;
extern DataType *typ_CE_230;
extern DataTypeComponent *typ_CF_230_entries;
extern DataType *typ_CF_230;
extern DataTypeComponent *typ_CK_230_entries;
extern DataType *typ_CK_230;
extern DataTypeComponent *typ_CM_ABS_RANGE_230_entries;
extern DataType *typ_CM_ABS_RANGE_230;
extern DataTypeComponent *typ_CM_AUI_230_entries;
extern DataType *typ_CM_AUI_230;
extern DataTypeComponent *typ_CM_CCD_230_entries;
extern DataType *typ_CM_CCD_230;
extern DataTypeComponent *typ_CM_CCP_230_entries;
extern DataType *typ_CM_CCP_230;
extern DataTypeComponent *typ_CM_CD_ELECTRODE_230_entries;
extern DataType *typ_CM_CD_ELECTRODE_230;
extern DataTypeComponent *typ_CM_CSU_230_entries;
extern DataType *typ_CM_CSU_230;
extern DataTypeComponent *typ_CM_DDI_230_entries;
extern DataType *typ_CM_DDI_230;
extern DataTypeComponent *typ_CM_DIN_230_entries;
extern DataType *typ_CM_DIN_230;
extern DataTypeComponent *typ_CM_DLD_230_entries;
extern DataType *typ_CM_DLD_230;
extern DataTypeComponent *typ_CM_DLT_230_entries;
extern DataType *typ_CM_DLT_230;
extern DataTypeComponent *typ_CM_DTN_230_entries;
extern DataType *typ_CM_DTN_230;
extern DataTypeComponent *typ_CM_EIP_230_entries;
extern DataType *typ_CM_EIP_230;
extern DataTypeComponent *typ_CM_ELD_230_entries;
extern DataType *typ_CM_ELD_230;
extern DataTypeComponent *typ_CM_MDV_230_entries;
extern DataType *typ_CM_MDV_230;
extern DataTypeComponent *typ_CM_MOC_230_entries;
extern DataType *typ_CM_MOC_230;
extern DataTypeComponent *typ_CM_MSG_230_entries;
extern DataType *typ_CM_MSG_230;
extern DataTypeComponent *typ_CM_NDL_230_entries;
extern DataType *typ_CM_NDL_230;
extern DataTypeComponent *typ_CM_OCD_230_entries;
extern DataType *typ_CM_OCD_230;
extern DataTypeComponent *typ_CM_OSD_230_entries;
extern DataType *typ_CM_OSD_230;
extern DataTypeComponent *typ_CM_OSP_230_entries;
extern DataType *typ_CM_OSP_230;
extern DataTypeComponent *typ_CM_PCF_230_entries;
extern DataType *typ_CM_PCF_230;
extern DataTypeComponent *typ_CM_PEN_230_entries;
extern DataType *typ_CM_PEN_230;
extern DataTypeComponent *typ_CM_PI_230_entries;
extern DataType *typ_CM_PI_230;
extern DataTypeComponent *typ_CM_PIP_230_entries;
extern DataType *typ_CM_PIP_230;
extern DataTypeComponent *typ_CM_PLN_230_entries;
extern DataType *typ_CM_PLN_230;
extern DataTypeComponent *typ_CM_PRL_230_entries;
extern DataType *typ_CM_PRL_230;
extern DataTypeComponent *typ_CM_PTA_230_entries;
extern DataType *typ_CM_PTA_230;
extern DataTypeComponent *typ_CM_RANGE_230_entries;
extern DataType *typ_CM_RANGE_230;
extern DataTypeComponent *typ_CM_RFR_230_entries;
extern DataType *typ_CM_RFR_230;
extern DataTypeComponent *typ_CM_RMC_230_entries;
extern DataType *typ_CM_RMC_230;
extern DataTypeComponent *typ_CM_SPD_230_entries;
extern DataType *typ_CM_SPD_230;
extern DataTypeComponent *typ_CM_SPS_230_entries;
extern DataType *typ_CM_SPS_230;
extern DataTypeComponent *typ_CM_UVC_230_entries;
extern DataType *typ_CM_UVC_230;
extern DataTypeComponent *typ_CM_VR_230_entries;
extern DataType *typ_CM_VR_230;
extern DataTypeComponent *typ_CM_WVI_230_entries;
extern DataType *typ_CM_WVI_230;
extern DataTypeComponent *typ_CN_230_entries;
extern DataType *typ_CN_230;
extern DataTypeComponent *typ_CNE_230_entries;
extern DataType *typ_CNE_230;
extern DataTypeComponent *typ_CP_230_entries;
extern DataType *typ_CP_230;
extern DataTypeComponent *typ_CQ_230_entries;
extern DataType *typ_CQ_230;
extern DataTypeComponent *typ_CX_230_entries;
extern DataType *typ_CX_230;
extern DataTypeComponent *typ_DLN_230_entries;
extern DataType *typ_DLN_230;
extern DataTypeComponent *typ_DR_230_entries;
extern DataType *typ_DR_230;
extern DataTypeComponent *typ_DT_230_entries;
extern DataType *typ_DT_230;
extern DataTypeComponent *typ_DTM_230_entries;
extern DataType *typ_DTM_230;
extern DataTypeComponent *typ_ED_230_entries;
extern DataType *typ_ED_230;
extern DataTypeComponent *typ_EI_230_entries;
extern DataType *typ_EI_230;
extern DataTypeComponent *typ_ELD_230_entries;
extern DataType *typ_ELD_230;
extern DataTypeComponent *typ_FC_230_entries;
extern DataType *typ_FC_230;
extern DataTypeComponent *typ_FN_230_entries;
extern DataType *typ_FN_230;
extern DataTypeComponent *typ_FT_230_entries;
extern DataType *typ_FT_230;
extern DataTypeComponent *typ_HD_230_entries;
extern DataType *typ_HD_230;
extern DataTypeComponent *typ_ID_230_entries;
extern DataType *typ_ID_230;
extern DataTypeComponent *typ_IS_230_entries;
extern DataType *typ_IS_230;
extern DataTypeComponent *typ_JCC_230_entries;
extern DataType *typ_JCC_230;
extern DataTypeComponent *typ_LA1_230_entries;
extern DataType *typ_LA1_230;
extern DataTypeComponent *typ_LA2_230_entries;
extern DataType *typ_LA2_230;
extern DataTypeComponent *typ_MA_230_entries;
extern DataType *typ_MA_230;
extern DataTypeComponent *typ_MO_230_entries;
extern DataType *typ_MO_230;
extern DataTypeComponent *typ_NA_230_entries;
extern DataType *typ_NA_230;
extern DataTypeComponent *typ_NM_230_entries;
extern DataType *typ_NM_230;
extern DataTypeComponent *typ_PL_230_entries;
extern DataType *typ_PL_230;
extern DataTypeComponent *typ_PN_230_entries;
extern DataType *typ_PN_230;
extern DataTypeComponent *typ_PPN_230_entries;
extern DataType *typ_PPN_230;
extern DataTypeComponent *typ_PT_230_entries;
extern DataType *typ_PT_230;
extern DataTypeComponent *typ_PTS_230_entries;
extern DataType *typ_PTS_230;
extern DataTypeComponent *typ_QIP_230_entries;
extern DataType *typ_QIP_230;
extern DataTypeComponent *typ_QSC_230_entries;
extern DataType *typ_QSC_230;
extern DataTypeComponent *typ_RCD_230_entries;
extern DataType *typ_RCD_230;
extern DataTypeComponent *typ_RI_230_entries;
extern DataType *typ_RI_230;
extern DataTypeComponent *typ_RP_230_entries;
extern DataType *typ_RP_230;
extern DataTypeComponent *typ_SCV_230_entries;
extern DataType *typ_SCV_230;
extern DataTypeComponent *typ_SI_230_entries;
extern DataType *typ_SI_230;
extern DataTypeComponent *typ_SN_230_entries;
extern DataType *typ_SN_230;
extern DataTypeComponent *typ_ST_230_entries;
extern DataType *typ_ST_230;
extern DataTypeComponent *typ_TM_230_entries;
extern DataType *typ_TM_230;
extern DataTypeComponent *typ_TN_230_entries;
extern DataType *typ_TN_230;
extern DataTypeComponent *typ_TQ_230_entries;
extern DataType *typ_TQ_230;
extern DataTypeComponent *typ_TS_230_entries;
extern DataType *typ_TS_230;
extern DataTypeComponent *typ_TX_230_entries;
extern DataType *typ_TX_230;
extern DataTypeComponent *typ_VARIES_230_entries;
extern DataType *typ_VARIES_230;
extern DataTypeComponent *typ_VH_230_entries;
extern DataType *typ_VH_230;
extern DataTypeComponent *typ_VID_230_entries;
extern DataType *typ_VID_230;
extern DataTypeComponent *typ_XAD_230_entries;
extern DataType *typ_XAD_230;
extern DataTypeComponent *typ_XCN_230_entries;
extern DataType *typ_XCN_230;
extern DataTypeComponent *typ_XON_230_entries;
extern DataType *typ_XON_230;
extern DataTypeComponent *typ_XPN_230_entries;
extern DataType *typ_XPN_230;
extern DataTypeComponent *typ_XTN_230_entries;
extern DataType *typ_XTN_230;

DataTypeList *HL7SCHNS_get_types_230();
#ifdef __cplusplus
}
#endif

#endif // _TYPES_230_H_
