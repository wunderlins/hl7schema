#ifndef _TYPES_210_H_
#define _TYPES_210_H_
#ifdef __cplusplus
extern "C" {
#endif

extern DataTypeComponent *typ_AD_210_entries;
extern DataType *typ_AD_210;
extern DataTypeComponent *typ_CE_210_entries;
extern DataType *typ_CE_210;
extern DataTypeComponent *typ_CE_0057_210_entries;
extern DataType *typ_CE_0057_210;
extern DataTypeComponent *typ_CK_210_entries;
extern DataType *typ_CK_210;
extern DataTypeComponent *typ_CM_210_entries;
extern DataType *typ_CM_210;
extern DataTypeComponent *typ_CM_UNDEFINED_210_entries;
extern DataType *typ_CM_UNDEFINED_210;
extern DataTypeComponent *typ_CN_210_entries;
extern DataType *typ_CN_210;
extern DataTypeComponent *typ_COMP_ID_DIGIT_210_entries;
extern DataType *typ_COMP_ID_DIGIT_210;
extern DataTypeComponent *typ_COMP_ID_NAME_210_entries;
extern DataType *typ_COMP_ID_NAME_210;
extern DataTypeComponent *typ_COMP_QUANT_210_entries;
extern DataType *typ_COMP_QUANT_210;
extern DataTypeComponent *typ_CQ_210_entries;
extern DataType *typ_CQ_210;
extern DataTypeComponent *typ_DT_210_entries;
extern DataType *typ_DT_210;
extern DataTypeComponent *typ_FT_210_entries;
extern DataType *typ_FT_210;
extern DataTypeComponent *typ_ID_210_entries;
extern DataType *typ_ID_210;
extern DataTypeComponent *typ_NM_210_entries;
extern DataType *typ_NM_210;
extern DataTypeComponent *typ_PN_210_entries;
extern DataType *typ_PN_210;
extern DataTypeComponent *typ_SET_ID_210_entries;
extern DataType *typ_SET_ID_210;
extern DataTypeComponent *typ_SI_210_entries;
extern DataType *typ_SI_210;
extern DataTypeComponent *typ_ST_210_entries;
extern DataType *typ_ST_210;
extern DataTypeComponent *typ_TM_210_entries;
extern DataType *typ_TM_210;
extern DataTypeComponent *typ_TN_210_entries;
extern DataType *typ_TN_210;
extern DataTypeComponent *typ_TS_210_entries;
extern DataType *typ_TS_210;
extern DataTypeComponent *typ_TX_210_entries;
extern DataType *typ_TX_210;

DataTypeList *HL7SCHNS_get_types_210();
#ifdef __cplusplus
}
#endif

#endif // _TYPES_210_H_
