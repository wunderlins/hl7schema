#include "api.h"
#include "types_210.h"
static DataType *HL7SCHNS_type_list_210 = NULL;
static DataTypeList *HL7SCHNS_type_210 = NULL;

DataTypeComponent *typ_AD_210_entries;
DataType *typ_AD_210;
DataTypeComponent *typ_CE_210_entries;
DataType *typ_CE_210;
DataTypeComponent *typ_CE_0057_210_entries;
DataType *typ_CE_0057_210;
DataTypeComponent *typ_CK_210_entries;
DataType *typ_CK_210;
DataTypeComponent *typ_CM_210_entries;
DataType *typ_CM_210;
DataTypeComponent *typ_CM_UNDEFINED_210_entries;
DataType *typ_CM_UNDEFINED_210;
DataTypeComponent *typ_CN_210_entries;
DataType *typ_CN_210;
DataTypeComponent *typ_COMP_ID_DIGIT_210_entries;
DataType *typ_COMP_ID_DIGIT_210;
DataTypeComponent *typ_COMP_ID_NAME_210_entries;
DataType *typ_COMP_ID_NAME_210;
DataTypeComponent *typ_COMP_QUANT_210_entries;
DataType *typ_COMP_QUANT_210;
DataTypeComponent *typ_CQ_210_entries;
DataType *typ_CQ_210;
DataTypeComponent *typ_DT_210_entries;
DataType *typ_DT_210;
DataTypeComponent *typ_FT_210_entries;
DataType *typ_FT_210;
DataTypeComponent *typ_ID_210_entries;
DataType *typ_ID_210;
DataTypeComponent *typ_NM_210_entries;
DataType *typ_NM_210;
DataTypeComponent *typ_PN_210_entries;
DataType *typ_PN_210;
DataTypeComponent *typ_SET_ID_210_entries;
DataType *typ_SET_ID_210;
DataTypeComponent *typ_SI_210_entries;
DataType *typ_SI_210;
DataTypeComponent *typ_ST_210_entries;
DataType *typ_ST_210;
DataTypeComponent *typ_TM_210_entries;
DataType *typ_TM_210;
DataTypeComponent *typ_TN_210_entries;
DataType *typ_TN_210;
DataTypeComponent *typ_TS_210_entries;
DataType *typ_TS_210;
DataTypeComponent *typ_TX_210_entries;
DataType *typ_TX_210;

DataTypeList *HL7SCHNS_get_types_210() {
if (HL7SCHNS_type_210 != NULL) return HL7SCHNS_type_210;
HL7SCHNS_type_list_210 = malloc(sizeof(DataType) * 23);
HL7SCHNS_type_210 = malloc(sizeof(DataTypeList));
typ_AD_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_AD_210 = (DataType*) malloc(sizeof(DataType));
typ_AD_210->id = "AD";
typ_AD_210->name = "";
typ_AD_210->length = 0;
typ_AD_210->childCount = 0;
typ_AD_210->component = typ_AD_210_entries;

typ_CE_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 6);
typ_CE_210 = (DataType*) malloc(sizeof(DataType));
typ_CE_210->id = "CE";
typ_CE_210->name = "";
typ_CE_210->length = 0;
typ_CE_210->childCount = 6;
typ_CE_210->component = typ_CE_210_entries;

typ_CE_0057_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_CE_0057_210 = (DataType*) malloc(sizeof(DataType));
typ_CE_0057_210->id = "CE_0057";
typ_CE_0057_210->name = "";
typ_CE_0057_210->length = 0;
typ_CE_0057_210->childCount = 0;
typ_CE_0057_210->component = typ_CE_0057_210_entries;

typ_CK_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_CK_210 = (DataType*) malloc(sizeof(DataType));
typ_CK_210->id = "CK";
typ_CK_210->name = "";
typ_CK_210->length = 0;
typ_CK_210->childCount = 0;
typ_CK_210->component = typ_CK_210_entries;

typ_CM_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_CM_210 = (DataType*) malloc(sizeof(DataType));
typ_CM_210->id = "CM";
typ_CM_210->name = "";
typ_CM_210->length = 0;
typ_CM_210->childCount = 0;
typ_CM_210->component = typ_CM_210_entries;

typ_CM_UNDEFINED_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_CM_UNDEFINED_210 = (DataType*) malloc(sizeof(DataType));
typ_CM_UNDEFINED_210->id = "CM_UNDEFINED";
typ_CM_UNDEFINED_210->name = "";
typ_CM_UNDEFINED_210->length = 0;
typ_CM_UNDEFINED_210->childCount = 0;
typ_CM_UNDEFINED_210->component = typ_CM_UNDEFINED_210_entries;

typ_CN_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_CN_210 = (DataType*) malloc(sizeof(DataType));
typ_CN_210->id = "CN";
typ_CN_210->name = "";
typ_CN_210->length = 0;
typ_CN_210->childCount = 0;
typ_CN_210->component = typ_CN_210_entries;

typ_COMP_ID_DIGIT_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_COMP_ID_DIGIT_210 = (DataType*) malloc(sizeof(DataType));
typ_COMP_ID_DIGIT_210->id = "COMP_ID_DIGIT";
typ_COMP_ID_DIGIT_210->name = "";
typ_COMP_ID_DIGIT_210->length = 0;
typ_COMP_ID_DIGIT_210->childCount = 0;
typ_COMP_ID_DIGIT_210->component = typ_COMP_ID_DIGIT_210_entries;

typ_COMP_ID_NAME_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_COMP_ID_NAME_210 = (DataType*) malloc(sizeof(DataType));
typ_COMP_ID_NAME_210->id = "COMP_ID_NAME";
typ_COMP_ID_NAME_210->name = "";
typ_COMP_ID_NAME_210->length = 0;
typ_COMP_ID_NAME_210->childCount = 0;
typ_COMP_ID_NAME_210->component = typ_COMP_ID_NAME_210_entries;

typ_COMP_QUANT_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_COMP_QUANT_210 = (DataType*) malloc(sizeof(DataType));
typ_COMP_QUANT_210->id = "COMP_QUANT";
typ_COMP_QUANT_210->name = "";
typ_COMP_QUANT_210->length = 0;
typ_COMP_QUANT_210->childCount = 0;
typ_COMP_QUANT_210->component = typ_COMP_QUANT_210_entries;

typ_CQ_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_CQ_210 = (DataType*) malloc(sizeof(DataType));
typ_CQ_210->id = "CQ";
typ_CQ_210->name = "";
typ_CQ_210->length = 0;
typ_CQ_210->childCount = 0;
typ_CQ_210->component = typ_CQ_210_entries;

typ_DT_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_DT_210 = (DataType*) malloc(sizeof(DataType));
typ_DT_210->id = "DT";
typ_DT_210->name = "";
typ_DT_210->length = 0;
typ_DT_210->childCount = 0;
typ_DT_210->component = typ_DT_210_entries;

typ_FT_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_FT_210 = (DataType*) malloc(sizeof(DataType));
typ_FT_210->id = "FT";
typ_FT_210->name = "";
typ_FT_210->length = 0;
typ_FT_210->childCount = 0;
typ_FT_210->component = typ_FT_210_entries;

typ_ID_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_ID_210 = (DataType*) malloc(sizeof(DataType));
typ_ID_210->id = "ID";
typ_ID_210->name = "";
typ_ID_210->length = 0;
typ_ID_210->childCount = 0;
typ_ID_210->component = typ_ID_210_entries;

typ_NM_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_NM_210 = (DataType*) malloc(sizeof(DataType));
typ_NM_210->id = "NM";
typ_NM_210->name = "";
typ_NM_210->length = 0;
typ_NM_210->childCount = 0;
typ_NM_210->component = typ_NM_210_entries;

typ_PN_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_PN_210 = (DataType*) malloc(sizeof(DataType));
typ_PN_210->id = "PN";
typ_PN_210->name = "";
typ_PN_210->length = 0;
typ_PN_210->childCount = 0;
typ_PN_210->component = typ_PN_210_entries;

typ_SET_ID_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_SET_ID_210 = (DataType*) malloc(sizeof(DataType));
typ_SET_ID_210->id = "SET_ID";
typ_SET_ID_210->name = "";
typ_SET_ID_210->length = 0;
typ_SET_ID_210->childCount = 0;
typ_SET_ID_210->component = typ_SET_ID_210_entries;

typ_SI_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_SI_210 = (DataType*) malloc(sizeof(DataType));
typ_SI_210->id = "SI";
typ_SI_210->name = "";
typ_SI_210->length = 0;
typ_SI_210->childCount = 0;
typ_SI_210->component = typ_SI_210_entries;

typ_ST_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_ST_210 = (DataType*) malloc(sizeof(DataType));
typ_ST_210->id = "ST";
typ_ST_210->name = "";
typ_ST_210->length = 0;
typ_ST_210->childCount = 0;
typ_ST_210->component = typ_ST_210_entries;

typ_TM_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_TM_210 = (DataType*) malloc(sizeof(DataType));
typ_TM_210->id = "TM";
typ_TM_210->name = "";
typ_TM_210->length = 0;
typ_TM_210->childCount = 0;
typ_TM_210->component = typ_TM_210_entries;

typ_TN_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_TN_210 = (DataType*) malloc(sizeof(DataType));
typ_TN_210->id = "TN";
typ_TN_210->name = "";
typ_TN_210->length = 0;
typ_TN_210->childCount = 0;
typ_TN_210->component = typ_TN_210_entries;

typ_TS_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_TS_210 = (DataType*) malloc(sizeof(DataType));
typ_TS_210->id = "TS";
typ_TS_210->name = "";
typ_TS_210->length = 0;
typ_TS_210->childCount = 0;
typ_TS_210->component = typ_TS_210_entries;

typ_TX_210_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * 0);
typ_TX_210 = (DataType*) malloc(sizeof(DataType));
typ_TX_210->id = "TX";
typ_TX_210->name = "";
typ_TX_210->length = 0;
typ_TX_210->childCount = 0;
typ_TX_210->component = typ_TX_210_entries;

typ_CE_210_entries[0] = (DataTypeComponent) {1, typ_ID_210, HL7SCHNS_string_727, "", 0, NULL, OPTIONAL};
typ_CE_210_entries[1] = (DataTypeComponent) {2, typ_ST_210, HL7SCHNS_string_970, "", 0, NULL, OPTIONAL};
typ_CE_210_entries[2] = (DataTypeComponent) {3, typ_ST_210, HL7SCHNS_string_772, "", 0, NULL, OPTIONAL};
typ_CE_210_entries[3] = (DataTypeComponent) {4, typ_ST_210, HL7SCHNS_string_551, "", 0, NULL, OPTIONAL};
typ_CE_210_entries[4] = (DataTypeComponent) {5, typ_ST_210, HL7SCHNS_string_552, "", 0, NULL, OPTIONAL};
typ_CE_210_entries[5] = (DataTypeComponent) {6, typ_ST_210, HL7SCHNS_string_771, "", 0, NULL, OPTIONAL};
HL7SCHNS_type_list_210[0] = *typ_AD_210;
HL7SCHNS_type_list_210[1] = *typ_CE_210;
HL7SCHNS_type_list_210[2] = *typ_CE_0057_210;
HL7SCHNS_type_list_210[3] = *typ_CK_210;
HL7SCHNS_type_list_210[4] = *typ_CM_210;
HL7SCHNS_type_list_210[5] = *typ_CM_UNDEFINED_210;
HL7SCHNS_type_list_210[6] = *typ_CN_210;
HL7SCHNS_type_list_210[7] = *typ_COMP_ID_DIGIT_210;
HL7SCHNS_type_list_210[8] = *typ_COMP_ID_NAME_210;
HL7SCHNS_type_list_210[9] = *typ_COMP_QUANT_210;
HL7SCHNS_type_list_210[10] = *typ_CQ_210;
HL7SCHNS_type_list_210[11] = *typ_DT_210;
HL7SCHNS_type_list_210[12] = *typ_FT_210;
HL7SCHNS_type_list_210[13] = *typ_ID_210;
HL7SCHNS_type_list_210[14] = *typ_NM_210;
HL7SCHNS_type_list_210[15] = *typ_PN_210;
HL7SCHNS_type_list_210[16] = *typ_SET_ID_210;
HL7SCHNS_type_list_210[17] = *typ_SI_210;
HL7SCHNS_type_list_210[18] = *typ_ST_210;
HL7SCHNS_type_list_210[19] = *typ_TM_210;
HL7SCHNS_type_list_210[20] = *typ_TN_210;
HL7SCHNS_type_list_210[21] = *typ_TS_210;
HL7SCHNS_type_list_210[22] = *typ_TX_210;
HL7SCHNS_type_210->length = 23;
HL7SCHNS_type_210->items = HL7SCHNS_type_list_210;
return HL7SCHNS_type_210;
}
