#include "api.h"

// linked list of all supported hl7 version
hl7type_handle version_280 = {280, "2.8", NULL, &HL7SCHNS_get_table_280, &HL7SCHNS_get_types_280, &HL7SCHNS_get_seg_280};
hl7type_handle version_271 = {271, "2.7.1", &version_280, &HL7SCHNS_get_table_271, &HL7SCHNS_get_types_271, &HL7SCHNS_get_seg_271};
hl7type_handle version_270 = {270, "2.7", &version_271, &HL7SCHNS_get_table_270, &HL7SCHNS_get_types_270, &HL7SCHNS_get_seg_270};
hl7type_handle version_260 = {260, "2.6", &version_270, &HL7SCHNS_get_table_260, &HL7SCHNS_get_types_260, &HL7SCHNS_get_seg_260};
hl7type_handle version_251 = {251, "2.5.1", &version_260, &HL7SCHNS_get_table_251, &HL7SCHNS_get_types_251, &HL7SCHNS_get_seg_251};
hl7type_handle version_250 = {250, "2.5", &version_251, &HL7SCHNS_get_table_250, &HL7SCHNS_get_types_250, &HL7SCHNS_get_seg_250};
hl7type_handle version_240 = {240, "2.4", &version_250, &HL7SCHNS_get_table_240, &HL7SCHNS_get_types_240, &HL7SCHNS_get_seg_240};
hl7type_handle version_231 = {231, "2.3.1", &version_240, &HL7SCHNS_get_table_231, &HL7SCHNS_get_types_231, &HL7SCHNS_get_seg_231};
hl7type_handle version_230 = {230, "2.3", &version_231, &HL7SCHNS_get_table_230, &HL7SCHNS_get_types_230, &HL7SCHNS_get_seg_230};
hl7type_handle version_220 = {220, "2.2", &version_230, &HL7SCHNS_get_table_220, &HL7SCHNS_get_types_220, &HL7SCHNS_get_seg_220};
hl7type_handle version_210 = {210, "2.1", &version_220, &HL7SCHNS_get_table_210, &HL7SCHNS_get_types_210, &HL7SCHNS_get_seg_210};
hl7type_handle *version = &version_210;
