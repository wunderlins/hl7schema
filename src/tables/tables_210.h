#ifndef _TABLES_210_H_
#define _TABLES_210_H_
#ifdef __cplusplus
extern "C" {
#endif

TableList *HL7SCHNS_get_table_210();
#ifdef __cplusplus
}
#endif

#endif // _TABLES_210_H_
