#ifndef _TABLES_231_H_
#define _TABLES_231_H_
#ifdef __cplusplus
extern "C" {
#endif

TableList *HL7SCHNS_get_table_231();
#ifdef __cplusplus
}
#endif

#endif // _TABLES_231_H_
