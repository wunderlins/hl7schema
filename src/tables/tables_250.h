#ifndef _TABLES_250_H_
#define _TABLES_250_H_
#ifdef __cplusplus
extern "C" {
#endif

TableList *HL7SCHNS_get_table_250();
#ifdef __cplusplus
}
#endif

#endif // _TABLES_250_H_
