#ifndef _TABLES_260_H_
#define _TABLES_260_H_
#ifdef __cplusplus
extern "C" {
#endif

TableList *HL7SCHNS_get_table_260();
#ifdef __cplusplus
}
#endif

#endif // _TABLES_260_H_
