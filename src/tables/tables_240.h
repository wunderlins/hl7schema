#ifndef _TABLES_240_H_
#define _TABLES_240_H_
#ifdef __cplusplus
extern "C" {
#endif

TableList *HL7SCHNS_get_table_240();
#ifdef __cplusplus
}
#endif

#endif // _TABLES_240_H_
