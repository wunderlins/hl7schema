#ifndef _TABLES_251_H_
#define _TABLES_251_H_
#ifdef __cplusplus
extern "C" {
#endif

TableList *HL7SCHNS_get_table_251();
#ifdef __cplusplus
}
#endif

#endif // _TABLES_251_H_
