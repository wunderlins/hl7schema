#ifndef _TABLES_230_H_
#define _TABLES_230_H_
#ifdef __cplusplus
extern "C" {
#endif

TableList *HL7SCHNS_get_table_230();
#ifdef __cplusplus
}
#endif

#endif // _TABLES_230_H_
