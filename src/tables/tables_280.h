#ifndef _TABLES_280_H_
#define _TABLES_280_H_
#ifdef __cplusplus
extern "C" {
#endif

TableList *HL7SCHNS_get_table_280();
#ifdef __cplusplus
}
#endif

#endif // _TABLES_280_H_
