#ifndef _TABLES_220_H_
#define _TABLES_220_H_
#ifdef __cplusplus
extern "C" {
#endif

TableList *HL7SCHNS_get_table_220();
#ifdef __cplusplus
}
#endif

#endif // _TABLES_220_H_
