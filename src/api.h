/**
 * @file api.h
 * @author Simon Wunderlin (swunderlin@gmail.com)
 * @brief data types and api for hl7schmea shared object
 * @version 1.0.0
 * @date 2021-12-14
 * 
 * This Library provides a database of all HL7 definitions from version 2.1 up to version 2.8
 * (see `src/versions.c` for a list of all supported versions).
 * 
 * The database provides lookup functions for segments, fields, datatypes and tables for 
 * structure validation and description of hl7 files.
 * 
 * Usage examples can be obtained from the unit test file `src/test.c`.
 * 
 * @copyright Copyright (c) 2021, Simon Wunderlin
 */

#ifndef __HL7_API__
#define __HL7_API__

//#include "hl7types.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "strings.h"

#define HL7SCHNS_ HL7SCHEMA_NAMESPACE_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief optionality of elements
 * 
 * DataTypesComponent and Fields can be optional, required, etc.
 */
typedef enum Usage {
    //! may be ommited if there is no required element in front of this element
    OPTIONAL=0,
    //! must be declared
    REQUIRED,
    //! still declared but may be empty. needed for structural reasons
    COMPATIBILITY,
    //! conditionally required
    CONDITIONAL,
    //! not used anymore in this version
    WITHDRAWN,
    //! unspoorted
    NOT_SUPPORTED,
    //! must be declared but can be empty
    REQUIRED_EMPTY
} Usage;

typedef struct DataType DataType; // fwd decl
/**
 * @brief key/value table entry
 */
typedef struct TableEntry {
    const char *key;
    const char *value;
} TableEntry;

/**
 * @brief hl7 table
 * a collection of key/value entries
 */
typedef struct Table {
    //! upper case string identifier
    char *id;
    //! a short descriptive name
    char *name;
    //! description on usage of this table
    char *description;
    //! chapter in which the table is defined
    char *chapter;
    //! type of table (HL7, User, Local, etc.)
    char *type;
    //! number of define key/value pairs, may be 0 (e.g. User Tables)
    int childCount;
    //! array of key/value pairs
    TableEntry *entries;
} Table;

/**
 * @brief a list of tables
 */
typedef struct TableList {
    int length;
    Table *items;
} TableList;

/**
 * @brief item of a complex data type
 * Datatypes may have sub fields, the are defined as DataTypeComponent and 
 * then grouped togehter in DataType->component[]
 */
typedef struct DataTypeComponent {
    //! position in parents structure, starting with 1
    int pos;
    //! the base type of the sub component
    DataType *type;
    //! descriptive name of the component
    const char* name;
    //! optional description of the component
    const char* description;
    //! maximum length in bytes, 0 is infinite
    int length;
    //! optional table name, may be NULL
    char *table;
    //! usage information of the component is optional, required, etc.
    Usage usage;
} DataTypeComponent;

/**
 * @brief base data type
 * 
 * There are simple and complex datatypes. 
 * 
 * Simple (sclar): have no children and therefore `DataType->childCount` is 0. For example `ST` means string, and a string has no sub components.
 * Complex (lists): have child components. Each child has a type that may be complex again.
 */
typedef struct DataType {
    //! upper case string identifier
    const char* id;
    //! short name of the type
    const char* name;
    //! maximum length in bytes, 0 is infinite
    int length;
    //! how many DataTypeComponent, may be 0 for simple (scalar) types
    int childCount;
    //! list of DataTypeComponent
    DataTypeComponent *component;
} DataType;

/**
 * @brief a list of DataType
 */
typedef struct DataTypeList {
    int length;
    DataType *items;
} DataTypeList;

typedef struct Field Field; // fwd decl
/**
 * @brief hl7 segment
 */
typedef struct Segment {
    //! upper case string identifier, e.g. "PID" or "MSH"
    char *id;
    //! optional descriptive name
    const char *name;
    //! optional segment description
    const char *description;
    //! chapter, may be NULL
    const char *chapter;
    //! number of defined fields
    int childCount;
    //! list of fields
    Field *fields;
} Segment;

/**
 * @brief list of segments
 */
typedef struct SegmentList {
    int length;
    Segment *items;
} SegmentList;

/**
 * @brief element of a Segment
 */
typedef struct Field {
    //! position in segment starting with 1
    int pos;
    //! link to data type
    DataType *type;
    //! descriptive name of the field
    const char *name;
    //! number of possible repetitions, -1 is infinite
    int repeat;
    //! length in bytes, 0 is infinite
    int length;
    //! optional table name. This name may be looked up in tables. it may not be found in tables (user defined tables)
    char *table;
    //! required, optional, etc.
    Usage usage;
} Field;

/**
 * @brief linked list of supported versions
 * 
 * This structure contains a list of available versions in the shared object.
 * 
 * You can find out if a version is suported by using `get_version(251)`. 
 * If it does not return NULL you are golden.
 */
typedef struct hl7type_handle hl7type_handle; // fwd decl
typedef struct hl7type_handle {
    //! 3 digit version, "2.8" is 280, "2.5.12 is 251, etc.
    int id;
    //! string version, usually something like "2.5.1" or "2.1"
    char *version;
    //! link to next greater version, is NULL in the highest supported version
    hl7type_handle *next;

    //! function pointer to `get_table()` of this version
    TableList* (*get_table)(void);
    //! function pointer to `get_types()` of this version
    DataTypeList* (*get_types)(void);
    //! function pointer to `get_seg()` of this version
    SegmentList* (*get_seg)(void);

} hl7type_handle;

hl7type_handle *get_version_by_str(const char *vstr);

/**
 * @brief Get the version object
 * 
 * @param vint 
 * @return hl7type_handle* 
 */
hl7type_handle *get_version(int vint);

/**
 * @brief Get the table object
 * 
 * @param id 
 * @param version 
 * @return Table* 
 */
Table *get_table(const char *id, int version);

/**
 * @brief Get the type object
 * 
 * @param id 
 * @param version 
 * @return DataType* 
 */
DataType *get_type(const char *id, int version);

/*
 * @brief Get the segment object
 * 
 * @param segname 3 char segment name, must be uppser case
 * @return hl7segment*, NULL if not found
 */
Segment *get_segment(const char *id, int version);

#ifdef __cplusplus
}
#endif

#include "versions.h"

#endif // __HL7_API__
