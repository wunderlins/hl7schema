#include "tables/tables_210.h"
#include "types/types_210.h"
#include "segments/segments_210.h"
#include "tables/tables_220.h"
#include "types/types_220.h"
#include "segments/segments_220.h"
#include "tables/tables_230.h"
#include "types/types_230.h"
#include "segments/segments_230.h"
#include "tables/tables_231.h"
#include "types/types_231.h"
#include "segments/segments_231.h"
#include "tables/tables_240.h"
#include "types/types_240.h"
#include "segments/segments_240.h"
#include "tables/tables_250.h"
#include "types/types_250.h"
#include "segments/segments_250.h"
#include "tables/tables_251.h"
#include "types/types_251.h"
#include "segments/segments_251.h"
#include "tables/tables_260.h"
#include "types/types_260.h"
#include "segments/segments_260.h"
#include "tables/tables_270.h"
#include "types/types_270.h"
#include "segments/segments_270.h"
#include "tables/tables_271.h"
#include "types/types_271.h"
#include "segments/segments_271.h"
#include "tables/tables_280.h"
#include "types/types_280.h"
#include "segments/segments_280.h"
extern hl7type_handle version_280;
extern hl7type_handle version_271;
extern hl7type_handle version_270;
extern hl7type_handle version_260;
extern hl7type_handle version_251;
extern hl7type_handle version_250;
extern hl7type_handle version_240;
extern hl7type_handle version_231;
extern hl7type_handle version_230;
extern hl7type_handle version_220;
extern hl7type_handle version_210;
extern hl7type_handle *version;
