#include <stdio.h>
#include "CUnit/Basic.h"
#include "api.h"
#include "versions.h"

void test_versions() {
    int i = 0;
    hl7type_handle *v = version;
    TableList *tl = NULL;
    DataTypeList *dtl = NULL;
    SegmentList *sl = NULL;
    
    // check function pointers of version
    while (v->next) {
        tl = v->get_table();
        dtl = v->get_types();
        sl = v->get_seg();
        if (v->id == 210) {
            //printf("210 length: 128\n");
            CU_ASSERT_EQUAL(128, tl->length);
            CU_ASSERT_EQUAL(23, dtl->length);
            CU_ASSERT_EQUAL(38, sl->length);
        }
        v = v->next;
        i++;
    }
    CU_ASSERT_EQUAL(10, i);
}

void test_tables() {

    Table *t0001 = get_table("0001", 210);
    CU_ASSERT_EQUAL(4, t0001->childCount);

    TableEntry *t = t0001->entries;
    CU_ASSERT_EQUAL(0, strcmp(t[0].key, "F"));
    CU_ASSERT_EQUAL(0, strcmp(t[0].value, "Female"));
    CU_ASSERT_EQUAL(0, strcmp(t[1].key, "M"));
    CU_ASSERT_EQUAL(0, strcmp(t[1].value, "Male"));
    CU_ASSERT_EQUAL(0, strcmp(t[2].key, "O"));
    CU_ASSERT_EQUAL(0, strcmp(t[2].value, "Other"));
    CU_ASSERT_EQUAL(0, strcmp(t[3].key, "U"));
    CU_ASSERT_EQUAL(0, strcmp(t[3].value, "Unknown"));
}

void test_pid_segment() {
    Segment *pid = get_segment("PID", 251);
    CU_ASSERT_NOT_EQUAL_FATAL(pid, NULL);

    // check number of elements
    printf("length: %d ", pid->childCount);
    CU_ASSERT_EQUAL(39, pid->childCount); // should have 40 fields for 2.5.1

    // unknown segment
    //Segment *xxx = get_segment("XXX", 210);
    //CU_ASSERT_EQUAL(NULL, xxx);
}

void test_msh_segment() {
    int v = 280;

    // test version 2.8
    hl7type_handle *ver = get_version(v);
    CU_ASSERT_NOT_EQUAL(NULL, ver);

    // get MSH segment
    Segment *msh = get_segment("MSH", v);
    Field *fields = msh->fields;
    Field msh3 = fields[2];

    // test msh-3
    CU_ASSERT_EQUAL(0, strcmp(fields[2].name, "Sending Application"));
    CU_ASSERT_EQUAL(0, fields[2].length);
    CU_ASSERT_EQUAL(0, strcmp(fields[2].type->id, "HD"));
    CU_ASSERT_EQUAL(OPTIONAL, fields[2].usage);
    CU_ASSERT_EQUAL(1, msh->fields[2].repeat); 
    CU_ASSERT_EQUAL(0, strcmp(fields[2].table, "0361"));
    Table *t0361 = get_table(fields[2].table, v);
    CU_ASSERT_EQUAL(0, t0361->childCount);

    // test msh-3's data type
    //DataType *hd = get_type(msh3.type, v);
    //CU_ASSERT_NOT_EQUAL(NULL, hd);

    DataType *hd = msh3.type;
    DataTypeComponent hd2 = hd->component[1];
    CU_ASSERT_EQUAL(2, hd2.pos);
    CU_ASSERT_EQUAL(0, strcmp(hd2.type->id, "ST"));
    CU_ASSERT_EQUAL(0, strcmp(hd2.name, "Universal ID"));
    CU_ASSERT_NOT_EQUAL(NULL, hd2.description);
    CU_ASSERT_EQUAL(0, hd2.length);
    //CU_ASSERT_EQUAL(NULL, hd2.table);
    CU_ASSERT_EQUAL(CONDITIONAL, hd2.usage);

}

int main() {
    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return (int) CU_get_error();

    // Sets the basic run mode, CU_BRM_VERBOSE will show maximum output of run details
    // Other choices are: CU_BRM_SILENT and CU_BRM_NORMAL
    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_pSuite pSuite = NULL;

    // Add a suite to the registry
    pSuite = CU_add_suite("address", 0, 0);
    // Always check if add was successful
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return (int) CU_get_error();
    }

    // test versions
    if (NULL == CU_add_test(pSuite, "test_versions", test_versions)) {
        CU_cleanup_registry();
        return (int) CU_get_error();
    }

    // test tables
    if (NULL == CU_add_test(pSuite, "test_tables", test_tables)) {
        CU_cleanup_registry();
        return (int) CU_get_error();
    }

    // test pid segment
    if (NULL == CU_add_test(pSuite, "test_pid_segment", test_pid_segment)) {
        CU_cleanup_registry();
        return (int) CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test_msh_segment", test_msh_segment)) {
        CU_cleanup_registry();
        return (int) CU_get_error();
    }

    // Run the tests and show the run summary
    CU_basic_run_tests();
    return (int) CU_get_number_of_tests_failed();
}
