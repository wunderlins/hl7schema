# Generate hl7 structures for C

This project generates data-structures that describe HL7 schemas. 
The goal is to expose structures tables for C (and maybe other languages) 
for quick lookup of HL7 structure information such as 
- Relation of Segments, Elements and Types
- Information (describtive Text) for Types and Elements
- Validation information (required, repeat, etc.)
- Minimal dependencies. the C implementation depends on `stdlib.h` and `CUnit` only

# TLDR; how to build

## cmake

We use cmake, and therefore the build process should be fairly straight forward 
on many platforms. A minimal dependency is CUnit, it needs to be installed on your system.

```bash
cmake -S src -B build
cmake --build ./build -j 4 # where 4 means use 4 cores
```

If you are on windows, you may use MinGW64 (other environments untested) like so:

```bash
cmake -S src/ -B build -G "MinGW Makefiles"
cmake --build ./build -j 4 # where 4 means use 4 cores
```

After the project is compiled, you will find the shared object in 
`build/lib7types.so` (or `*.dll` on windows or `*.dylb` on MacOS).
For usage of the shared object, you may want to copy `build/lib7types.h` 
as general guidance, api description and the datatypes into your project.

## gnu make

There is a simple gnu make file in the source directory, build the usual way:

```bash
cd src
make
make check && echo "compile and tests succeeded"
```

You will find a `lib7types.so` and `lib7types.h` file in the same folder 
as the Makefile. Copy them to the desired locations.

# How extend / develop the 7types library
## Step 0: (optional) get definitions

Definitions can be fetched with a scraper script, run it as follows:

```bash
cd data
./get.py
```

This will populate the `data/xml` forlder with tpyes, tables and segment definitions
of various hl7 versions. You don't have todo this, all files up to version 
2.8 are already checked in, just extract `xml2.tgz` like so:

```bash
cd data
tar xzf xml2.tgz
```

## Step 1: (optional) import xml structures into sqlite database

`./tools/import.sh` will import the downloaded xml structures into an sqlite database.
This makes it possible to optimizes and normalize the definitions. `import.sh` 
runs `dedup.py` to identify all strings and create a string database of 
unique strings. This reduces the final shared object's size to a third of it's size.

## Step 2: generate C source files

`./tools/generate.sh` will create all the data definition files from the database 
and store version specific files in `src/[table|type|segment]/*_<version>.c`.

Also, `versions.c|h` (containing metadata of the generated versions) and `strings.c` 
(containing a string database) will be created.

Together with `api.c|h` whis makes up the souce code for the shared object.

## Step 3: compile

For development purpose `./tools/build.sh` gives a good idea of how to run the compilation
process on various systems, have a look, adopt if needed.