#!/usr/bin/env python3

from data.db import dbConnection, escape, dbfile
import sys, os

versions = []
con = None
cur = None

def guard_open(name: str, version: int):
    name = name.upper()
    buffer  = f"#ifndef _{name}_{version}_H_\n"
    buffer += f"#define _{name}_{version}_H_\n"
    buffer += f"#ifdef __cplusplus\nextern \"C\" {{\n#endif\n\n"
    return buffer

def guard_close(name: str, version: int):
    buffer  = f"#ifdef __cplusplus\n}}\n#endif\n\n"
    buffer += f"#endif // _{name}_{version}_H_\n"
    return buffer

def c_str_escape(s: str):
    s = s.replace("\\", "\\\\")
    s = s.replace('"', "\\\"")
    s = s.replace("\r", "\\r")
    return s.replace("\n", "\\n")

def gen_strings():
    sql = "select count(pk) from \"Strings\""
    l = int(cur.execute(sql).fetchone()[0])


    sql = "select pk, string from \"Strings\""
    cur.execute(sql)

    # open file
    f = f"src/strings.c"
    fh = f"src/strings.h"
    fp = open(f, "w")
    fph = open(fh, "w")
    fp.write(f"\n")
    fph.write(guard_open("STRINGS", 0))

    for pk, string in cur:
        fp.write(f"const char *HL7SCHNS_string_{pk} = \"{c_str_escape(string)}\";\n")
        fph.write(f"extern const char *HL7SCHNS_string_{pk};\n")

    fp.write("\n")
    fp.close()
    fph.write(guard_close("STRINGS", 0))
    fph.close()

def gen_tables(version):
    print(f"Generating C table for {version} ...")

    # open file
    f = f"src/tables/tables_{version}.c"
    fh = f"src/tables/tables_{version}.h"
    fp = open(f, "w")
    fph = open(fh, "w")
    fph.write(guard_open("TABLES", version))

    sql = f"""
        select * 
        from "Tables"
        WHERE version = (select version from Version where id = '{version}')
    """
    tbls = cur.execute(sql).fetchall()

    fp.write(f"#include \"api.h\"\n#include \"tables_{version}.h\"\n")
    fp.write(f"static Table *HL7SCHNS_table_list_{version} = NULL;\n")
    fp.write(f"static TableList *HL7SCHNS_table_{version} = NULL;\n")
    fp.write(f"TableList *HL7SCHNS_get_table_{version}() {{\n")
    fp.write(f"if (HL7SCHNS_table_{version} != NULL) return HL7SCHNS_table_{version};\n")
    fp.write(f"HL7SCHNS_table_list_{version} = malloc(sizeof(Table) * {len(tbls)});\n")
    fp.write(f"HL7SCHNS_table_{version} = malloc(sizeof(TableList));\n\n")
    fph.write(f"TableList *HL7SCHNS_get_table_{version}();\n")

    tbl_meta = []
    
    for pk, id, name, description, chapter, type, childCount, vstr, name_ptr, description_ptr in tbls:
        #print(str(pk) + " " + name)
        sql = f"""
            SELECT count(pk) 
            FROM TableEntries
            WHERE id = '{id}' AND version = '{vstr}'
        """
        c = int(cur.execute(sql).fetchone()[0])
        fp.write(f"TableEntry *tbl_{id}_{version}_entries = malloc(sizeof(TableEntry) * {c});\n")
        sql = f"""
            SELECT * 
            FROM TableEntries
            WHERE id = '{id}' AND version = '{vstr}'
        """
        cur.execute(sql)
        c = 0
        for pk, id, value, description, ve, description_ptr in cur:
            fp.write(f"tbl_{id}_{version}_entries[{c}] = (TableEntry) ")
            if description_ptr:
                v = f"HL7SCHNS_string_{description_ptr}"
            elif description:
                v = f"\"{c_str_escape(description)}\""
            else:
                v = f"\"\""
            fp.write(f"""{{ "{value}", {v} }};\n""")
            c = c + 1
        
        tbl_meta.append({
            "id": id,
            "length": c
        })
        #fp.write("};\n")

        fp.write(f"Table *tbl_{id}_{version} = malloc(sizeof(Table));\n")
        fp.write(f"tbl_{id}_{version}->id = \"{id}\";\n")
        fp.write(f"tbl_{id}_{version}->name = \"{name}\";\n")
        if description != None:
            fp.write(f"tbl_{id}_{version}->description = \"{c_str_escape(description)}\";\n")
        else:
            fp.write(f"tbl_{id}_{version}->description = NULL;\n")
        fp.write(f"tbl_{id}_{version}->chapter = \"{c_str_escape(chapter)}\";\n")
        fp.write(f"tbl_{id}_{version}->type = \"{c_str_escape(type)}\";\n")
        fp.write(f"tbl_{id}_{version}->childCount = {c};\n")
        fp.write(f"tbl_{id}_{version}->entries = tbl_{id}_{version}_entries;\n")
        fp.write(f"\n")

    # create lookup table
    #fp.write(f"const Table *table_list_{version}[{len(tbl_meta)}] = {{ \n")
    #fp.write(f"const Table *table_list_{version}[{len(tbl_meta)}] = {{ \n")
    i = 0
    for e in tbl_meta:
        #fp.write(f"\ttbl_{e['id']}_{version},\n")    
        fp.write(f"HL7SCHNS_table_list_{version}[{i}] = *tbl_{e['id']}_{version};\n")
        i = i + 1
    #fp.write(f"}};\n")
    fp.write(f"HL7SCHNS_table_{version}->length = {len(tbl_meta)};\n")
    fp.write(f"HL7SCHNS_table_{version}->items = HL7SCHNS_table_list_{version};\n")
    fp.write(f"return HL7SCHNS_table_{version};\n}}\n")
    fp.close()
    fph.write(guard_close("TABLES", version))
    fph.close()


def gen_data_types(version): 
    print(f"Generating C types for {version} ...")

    # open file
    f = f"src/types/types_{version}.c"
    fh = f"src/types/types_{version}.h"
    fp = open(f, "w")
    fph = open(fh, "w")
    fph.write(guard_open("TYPES", version))

    sql = f"""
        select dt.*
        from DataType dt
        WHERE dt.version = (select version from Version where id = '{version}')
    """
    typs = cur.execute(sql).fetchall()

    fp.write(f"#include \"api.h\"\n#include \"types_{version}.h\"\n")
    fp.write(f"static DataType *HL7SCHNS_type_list_{version} = NULL;\n")
    fp.write(f"static DataTypeList *HL7SCHNS_type_{version} = NULL;\n\n")

    # exports
    for pk, idd, name, description, length, childCount, vstr, name_ptr, description_ptr in typs:
        fph.write(f"extern DataTypeComponent *typ_{idd}_{version}_entries;\n")
        fph.write(f"extern DataType *typ_{idd}_{version};\n")
        fp.write(f"DataTypeComponent *typ_{idd}_{version}_entries;\n")
        fp.write(f"DataType *typ_{idd}_{version};\n")
    fph.write("\n")
    fp.write("\n")

    # function decl
    fp.write(f"DataTypeList *HL7SCHNS_get_types_{version}() {{\n")
    fp.write(f"if (HL7SCHNS_type_{version} != NULL) return HL7SCHNS_type_{version};\n")
    fp.write(f"HL7SCHNS_type_list_{version} = malloc(sizeof(DataType) * {len(typs)});\n")
    fp.write(f"HL7SCHNS_type_{version} = malloc(sizeof(DataTypeList));\n")
    fph.write(f"DataTypeList *HL7SCHNS_get_types_{version}();\n")

    for pk, idd, name, description, length, childCount, vstr, name_ptr, description_ptr in typs:
        #fp.write(f"DataTypeComponent *typ_{idd}_{version}_entries = malloc(sizeof(DataTypeComponent) * {childCount});\n")
        fp.write(f"typ_{idd}_{version}_entries = (DataTypeComponent*) malloc(sizeof(DataTypeComponent) * {childCount});\n")
        n = "\"\""
        if description_ptr:
            n = f"HL7SCHNS_string_{name_ptr}"
        elif description:
            n = f"\"{c_str_escape(name)}\""
        #fp.write(f"DataType *typ_{idd}_{version} = malloc(sizeof(DataType));\n")
        fp.write(f"typ_{idd}_{version} = (DataType*) malloc(sizeof(DataType));\n")
        fp.write(f"typ_{idd}_{version}->id = \"{idd}\";\n")
        fp.write(f"typ_{idd}_{version}->name = {n};\n")
        fp.write(f"typ_{idd}_{version}->length = {length};\n")
        fp.write(f"typ_{idd}_{version}->childCount = {childCount};\n")
        fp.write(f"typ_{idd}_{version}->component = typ_{idd}_{version}_entries;\n\n")
        #}};\n")


    for pk, idd, name, description, length, childCount, vstr, name_ptr, description_ptr in typs:
        sql = f"""
            SELECT dt.*, fu.enum_name
            from DataTypeComponent dt JOIN FieldUsage fu on dt.usage = fu.id
            WHERE dt.id = '{idd}' AND dt.version = '{vstr}'
        """
        cur.execute(sql)
        c = 0

        for pkdt, id, pos, type, name, repeat, length, descr, table, usage, vstr , name_ptr, description_ptr, usage_enum in cur:
            fp.write(f"typ_{idd}_{version}_entries[{c}] = (DataTypeComponent) ")
            n = "\"\""
            d = "\"\""
            t = "NULL"
            if name_ptr:
                n = f"HL7SCHNS_string_{name_ptr}"
            elif name:
                n = f"\"{c_str_escape(name)}\""
            if description_ptr:
                d = f"HL7SCHNS_string_{description_ptr}"
            elif description:
                d = f"\"{c_str_escape(description)}\""
            if table:
                t = f"\"{table}\""
            fp.write(f"""{{{pos}, typ_{type}_{version}, {n}, {d}, {length}, {t}, {usage_enum}}};\n""")
            c = c + 1

    i = 0
    for pk, idd, name, description, length, childCount, vstr, name_ptr, description_ptr in typs:
        #fp.write(f"\ttbl_{e['id']}_{version},\n")    
        fp.write(f"HL7SCHNS_type_list_{version}[{i}] = *typ_{idd}_{version};\n")
        i = i + 1

    
    fp.write(f"HL7SCHNS_type_{version}->length = {len(typs)};\n")
    fp.write(f"HL7SCHNS_type_{version}->items = HL7SCHNS_type_list_{version};\n")
    fp.write(f"return HL7SCHNS_type_{version};\n}}\n")
    fp.close()
    fph.write(guard_close("TYPES", version))
    fph.close()

def gen_segments(version): 
    print(f"Generating C segments for {version} ...")

    sql = f"""
        select *
        from Segment
        WHERE version = (select version from Version where id = '{version}')
    """
    segs = cur.execute(sql).fetchall()

    # open file
    f = f"src/segments/segments_{version}.c"
    fh = f"src/segments/segments_{version}.h"
    fp = open(f, "w")
    fph = open(fh, "w")
    fph.write(guard_open("SEGMENTS", version))

    fp.write(f"#include \"api.h\"\n")
    fp.write(f"#include \"segments_{version}.h\"\n")
    fp.write(f"#include \"../types/types_{version}.h\"\n\n")

    fp.write(f"static Segment *HL7SCHNS_seg_list_{version} = NULL;\n")
    fp.write(f"static SegmentList *HL7SCHNS_seg_{version} = NULL;\n\n")
    fp.write(f"SegmentList *HL7SCHNS_get_seg_{version}() {{\n")
    fph.write(f"SegmentList *HL7SCHNS_get_seg_{version}();\n")
    fp.write(f"HL7SCHNS_get_types_{version}();\n")
    fp.write(f"if (HL7SCHNS_seg_{version} != NULL) return HL7SCHNS_seg_{version};\n")
    fp.write(f"HL7SCHNS_seg_list_{version} = malloc(sizeof(Segment) * {len(segs)});\n")
    fp.write(f"HL7SCHNS_seg_{version} = malloc(sizeof(SegmentList));\n")

    for pk, idd, name, description, chapter, childCount, vstr, name_ptr, description_ptr in segs:

        n = "\"\""
        d = "\"\""
        c = "\"\""
        if name_ptr:
            n = f"HL7SCHNS_string_{name_ptr}"
        elif name:
            n = f"\"{c_str_escape(name)}\""
        if description_ptr:
            d = f"HL7SCHNS_string_{description_ptr}"
        elif description:
            d = f"\"{c_str_escape(description)}\""
        if chapter:
            c = f"\"{c_str_escape(chapter)}\""

        fp.write(f"Field *seg_{idd}_{version}_entries = malloc(sizeof(Field) * {childCount});\n")
        fp.write(f"Segment *seg_{idd}_{version} = malloc(sizeof(Segment));\n")
        fp.write(f"seg_{idd}_{version}->id = \"{idd}\";\n")
        fp.write(f"seg_{idd}_{version}->name = {n};\n")
        fp.write(f"seg_{idd}_{version}->description = {d};\n")
        fp.write(f"seg_{idd}_{version}->chapter = {c};\n")
        fp.write(f"seg_{idd}_{version}->childCount = {childCount};\n")
        fp.write(f"seg_{idd}_{version}->fields = seg_{idd}_{version}_entries;\n\n")

        # add filed list entries
        sql = f"""
            SELECT f.*, fu.enum_name
            from Field f JOIN FieldUsage fu on f.usage = fu.id
            where f.id = '{idd}' AND f.version = '{vstr}'"""
        cur.execute(sql)
        c = 0
        for pk, id, pos, type, name, description, repeat, length, table, usage, vstr, name_ptr, enum_name in cur:
            
            n = "\"\""
            t = "NULL"
            if name_ptr:
                n = f"HL7SCHNS_string_{name_ptr}"
            elif name:
                n = f"\"{c_str_escape(name)}\""
            if table:
                t = f"\"{c_str_escape(table)}\""
            if repeat == "*":
                repeat = -1
            else:
                repeat = int(repeat)
            fp.write(f"seg_{idd}_{version}_entries[{c}] = (Field) {{{pos}, typ_{type}_{version}, {n}, {repeat}, {length}, {t}, {enum_name}}};\n")
            c = c + 1
    
    i = 0
    for pk, idd, name, description, chapter, childCount, vstr, name_ptr, description_ptr in segs:
        #fp.write(f"\ttbl_{e['id']}_{version},\n")    
        fp.write(f"HL7SCHNS_seg_list_{version}[{i}] = *seg_{idd}_{version};\n")
        i = i + 1

    fp.write(f"HL7SCHNS_seg_{version}->length = {len(segs)};\n")
    fp.write(f"HL7SCHNS_seg_{version}->items = HL7SCHNS_seg_list_{version};\n")
    fp.write(f"return HL7SCHNS_seg_{version};\n}}\n")
    fp.close()
    fph.write(guard_close("SEGMENTS", version))
    fph.close()

def main(argv):
    """ we expect a string with version numbers, space separated in argv[1] """
    global versions
    vs = argv[1].split(" ")
    for v in vs:
        vi = int(v.replace(".", ""))
        if vi < 100: vi = vi * 10
        versions.append(vi)
    #print(versions)

    # open handle to create our version linked list
    fpv = open("src/versions.c", "w")
    fpvh = open("src/versions.h", "w")
    fpv.write(f"""#include "api.h"\n\n// linked list of all supported hl7 version\n""")

    print(f"Generating C definitions for Strings ...")
    gen_strings()

    tbl_csrc = ""
    tbl_hsrc = ""
    typ_csrc = ""
    typ_hsrc = ""
    seg_csrc = ""
    seg_hsrc = ""
    next_version = None
    for v in versions:

        gen_tables(v)
        gen_data_types(v)
        gen_segments(v)
        tbl_csrc += f"\ttables/tables_{v}.c\n"
        tbl_hsrc += f"\ttables/tables_{v}.h\n"
        typ_csrc += f"\ttypes/types_{v}.c\n"
        typ_hsrc += f"\ttypes/types_{v}.h\n"
        seg_csrc += f"\tsegments/segments_{v}.c\n"
        seg_hsrc += f"\tsegments/segments_{v}.h\n"
        fpvh.write(f"#include \"tables/tables_{v}.h\"\n")
        fpvh.write(f"#include \"types/types_{v}.h\"\n")
        fpvh.write(f"#include \"segments/segments_{v}.h\"\n")

    versions.reverse()
    for v in versions:
        sql = f"""select version from Version where id = '{v}'"""
        vstr = cur.execute(sql).fetchone()[0]
        if next_version != None:
            #fpv.write(f"""{next_version}.next = &version_{v};\n""")
            fpv.write(f"""hl7type_handle version_{v} = {{{v}, "{vstr}", &{next_version}, &HL7SCHNS_get_table_{v}, &HL7SCHNS_get_types_{v}, &HL7SCHNS_get_seg_{v}}};\n""")
        else:
            fpv.write(f"""hl7type_handle version_{v} = {{{v}, "{vstr}", NULL, &HL7SCHNS_get_table_{v}, &HL7SCHNS_get_types_{v}, &HL7SCHNS_get_seg_{v}}};\n""")
        fpvh.write(f"extern hl7type_handle version_{v};\n")

        next_version = f"version_{v}"

    fpv.write(f"""hl7type_handle *version = &{next_version};\n""")
    fpvh.write("extern hl7type_handle *version;\n")
    fpv.close()
    fpvh.close()

    # cretae the cmake include file for tge generate versions
    f = f"src/cmake/tables.cmake"
    fp = open(f, "w")
    fp.write(f"SET(TABLES_C\n{tbl_csrc}\n)\n")
    fp.write(f"SET(TABLES_H\n{tbl_hsrc}\n)\n")
    fp.close()

    f = f"src/cmake/types.cmake"
    fp = open(f, "w")
    fp.write(f"SET(TYPES_C\n{typ_csrc}\n)\n")
    fp.write(f"SET(TYPES_H\n{typ_hsrc}\n)\n")
    fp.close()

    f = f"src/cmake/segments.cmake"
    fp = open(f, "w")
    fp.write(f"SET(SEG_C\n{seg_csrc}\n)\n")
    fp.write(f"SET(SEG_H\n{seg_csrc}\n)\n")
    fp.close()

if __name__ == "__main__":
    con, cur = dbConnection(dbfile)
    main(sys.argv)
