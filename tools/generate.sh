#!/usr/bin/env bash

GEN_C_CODE=1

. tools/versions.sh

# generate C structures
if [[ "$GEN_C_CODE" == "1" ]]; then
    ./generate.py "$versions"
fi
