#!/usr/bin/env bash

CREATEDB=1
IMPORT=1
DEDUP=1

. tools/versions.sh

# create db schema
if [[ "$CREATEDB" == "1" ]]; then
    rm data/schema2.db; sqlite3 data/schema2.db < data/schema2.sql
fi

# import xml definitions into database
if [[ "$IMPORT" == "1" ]]; then
    for v in $versions; do
        echo "Importing $v ..."
        ./import.py $v > import_$v.log
    done
fi

# deduplicate Strings
if [[ "$DEDUP" == "1" ]]; then
    echo "Deduplicating Strings, this may take some time ..."
    ./dedup.py
fi
