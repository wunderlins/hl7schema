#!/usr/bin/env bash

BUILD_TYPE=Debug # Debug|Release

COMPILE_C_CODE=1
TEST_C_CODE=1

. tools/versions.sh

if [[ "$COMPILE_C_CODE" == "1" ]]; then
    n=$(( $(grep -c ^processor /proc/cpuinfo) - 1 ))
    if [ "$(uname)" == "Darwin" ]; then
        n=$(( $(sysctl -n hw.ncpu) - 1 ))
        echo "$n"
    fi
    echo "Compiling C code in $n cores ..."
    if [ "$(uname -o)" == "Msys" ]; then
        cmake -S src -B build -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
            -G "MinGW Makefiles" -DCMAKE_C_COMPILER=clang.exe
    else
        cmake -S src -B build -DCMAKE_BUILD_TYPE=$BUILD_TYPE
    fi
    cmake --build ./build -j $n
fi

if [[ "$TEST_C_CODE" == "1" ]]; then
    cmake --build ./build --target test
fi
