#!/usr/bin/env python3

from data.db import dbConnection, escape, dbfile
import sys, os

con = None
cur = None

def dedup_row(tbl: str, field: str):
    first_row = 1
    try:
        first_row = int(cur.execute("SELECT SEQ from sqlite_sequence WHERE name='Strings'").fetchone()[0])+1
    except: pass
    sql = f"""
        select {field}
        from {tbl}
        WHERE {field} IS NOT NULL and {field} != ''
        GROUP BY UPPER({field});
    """
    cur.execute(sql)

    # loop over new strings
    sql = "INSERT INTO \"Strings\" (string) VALUES "
    strlist = []
    for row in cur:
        sql += f"({escape(row[0])}), "
        strlist.append(row[0])
        #print(row[0])
    sql = sql[:-2] + ";"
    
    con.execute(sql)
    con.commit()
    last_row = int(cur.execute("SELECT SEQ from sqlite_sequence WHERE name='Strings'").fetchone()[0])
    if first_row < last_row:
        print(f"{tbl}.{field}: added {last_row - first_row} records,  start {first_row}, end {last_row}")

        # update all strings with refertences
        i = first_row
        for s in strlist:
            sql = f"update {tbl} set {field}_ptr={i} WHERE UPPER({field}) = UPPER({escape(s)})"
            con.execute(sql)
            i = i + 1
        con.commit()
    else:
        print(f"{tbl}.{field}: No records added")

def dedup_datatype():
    dedup_row("DataType", "name")
    dedup_row("DataType", "description")
    
    dedup_row("DataTypeComponent", "name")
    dedup_row("DataTypeComponent", "description")

    dedup_row("Segment", "name")
    dedup_row("Segment", "description")

    dedup_row("Field", "name")
    dedup_row("TableEntries", "description")

    dedup_row("Tables", "name")
    dedup_row("Tables", "description")

    """
    first_row = int(cur.execute("SELECT SEQ from sqlite_sequence WHERE name='Strings'").fetchone()[0])+1
    sql = "" "
        select name
        from DataType
        WHERE name IS NOT NULL and NAME != ''
        GROUP BY UPPER(TRIM(name));
    "" "
    cur.execute(sql)

    # loop over new strings
    sql = "INSERT INTO \"Strings\" (string) VALUES "
    strlist = []
    for row in cur:
        sql += f"({escape(row[0])}), "
        strlist.append(row[0])
        #print(row[0])
    sql = sql[:-2] + ";"
    
    con.execute(sql)
    con.commit()
    last_row = int(cur.execute("SELECT SEQ from sqlite_sequence WHERE name='Strings'").fetchone()[0])
    if first_row < last_row:
        print(f"added {last_row - first_row} records,  start {first_row}, end {last_row}")

        # update all strings with refertences
        i = first_row
        for s in strlist:
            sql = f"update DataType set name_ptr={i} WHERE UPPER(name) = UPPER({escape(s)})"
            con.execute(sql)
            i = i + 1
        con.commit()
    else:
        print("No records added")
    """

def main(argv):
    # reset
    con.execute("DELETE FROM \"Strings\"")
    con.execute("UPDATE sqlite_sequence set SEQ=0 WHERE name='Strings'")
    con.commit()

    # start parsing datatypes
    dedup_datatype()

if __name__ == "__main__":
    con, cur = dbConnection(dbfile)
    main(sys.argv)
