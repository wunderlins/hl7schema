#!/usr/bin/env python3

from data.db import dbConnection, escape, dbfile
import xml.etree.ElementTree as ET
import sys
import re

version = None
con = None
cur = None
ns = None

def get_ns(file: str):
    return dict([node for _, node in ET.iterparse(file, events=['start-ns'])])

def get_node_by_tagname(parent, tagName, usens=''):
    return parent.findall(f".//{{{ns[usens]}}}{tagName}")

def get_content_by_tagname(parent, tagName, usens=''):
    content = None
    tag = parent.findall(f".//{{{ns[usens]}}}{tagName}")
    if len(tag):
        content = tag[0].text
    return content

def importDataTypes():
    global ns
    file = f"data/xml/{version}/DataTypes.xml"
    ns = get_ns(file)

    # open type list
    tree = ET.parse(file)
    root = tree.getroot()
    typeList = get_node_by_tagname(root, "ElementSummaryEnvelope")

    # parse datatypes
    for e in typeList:
        id = get_node_by_tagname(e, "Id")[0].text
        name = escape(get_content_by_tagname(e, "Label"))
        desc = escape(get_content_by_tagname(e, "Description"))

        # remove id from name
        name = re.sub(id + " ?- *", "", name)

        print(id + " - " + name)

        # parse details and lists
        file2 = f"data/xml/{version}/DataTypes/{id}.xml"
        tree = ET.parse(file2).getroot()

        #l = get_node_by_tagname(tree, "Length")[0].text
        l = tree.findall(f"./{{{ns['']}}}Length")[0].text
        children = get_node_by_tagname(tree, "FieldEnvelope")
        sql = f"""
            INSERT INTO "DataType"
            (id, name, description, length, childCount, version)
            VALUES
            ('{id}', {name}, {desc}, {l}, {len(children)}, '{version}')
        """
        con.execute(sql)
        con.commit()

        # loop over all children and import them
        for c in children:
            pos = get_node_by_tagname(c, "Id")[0].text.split(".")[1]
            tbl = escape(get_content_by_tagname(c, "TableId"))
            t = escape(get_content_by_tagname(c, "DataType"))
            name = escape(get_content_by_tagname(c, "Name"))
            repeat = escape(get_content_by_tagname(c, "Rpt"))
            desc = escape(get_content_by_tagname(c, "Description"))
            l = get_content_by_tagname(c, "Length")
            usage = escape(get_content_by_tagname(c, "Usage"))
            
            print(f"\t{id} {pos}")
            sql = f"""
                INSERT INTO "DataTypeComponent"
                (id, pos, type, name, description, "table", usage, version, length, repeat)
                VALUES
                ('{id}', {pos}, {t}, {name}, {desc}, {tbl}, {usage}, '{version}', {l}, {repeat})
            """
            con.execute(sql)
        con.commit()

def importSegments():
    global ns
    file = f"data/xml/{version}/Segments.xml"
    ns = get_ns(file)
    #print(ns)

    # open type list
    tree = ET.parse(file)
    root = tree.getroot()
    typeList = get_node_by_tagname(root, "ElementSummaryEnvelope")

    # parse datatypes
    for e in typeList:
        id = get_node_by_tagname(e, "Id")[0].text
        name = escape(get_content_by_tagname(e, "Label"))
        desc = escape(get_content_by_tagname(e, "Description"))
        chapt = ""
        try:
            chapt = get_node_by_tagname(e, "string", "d3p1")[0].text
        except: pass

        # remove id from name
        name = re.sub(id + " ?- *", "", name)

        print(id + " - " + name)

        # parse details and lists
        file2 = f"data/xml/{version}/Segments/{id}.xml"
        tree = ET.parse(file2).getroot()

        children = get_node_by_tagname(tree, "FieldEnvelope")
        sql = f"""
            INSERT INTO "Segment"
            (id, name, description, childCount, version, chapter)
            VALUES
            ('{id}', {name}, {desc}, {len(children)}, '{version}', {escape(chapt)})
        """
        con.execute(sql)
        con.commit()

        # loop over all children and import them
        for c in children:
            pos = get_node_by_tagname(c, "Id")[0].text.split(".")[1]
            tbl = escape(get_content_by_tagname(c, "TableId"))
            t = escape(get_content_by_tagname(c, "DataType"))
            name = escape(get_content_by_tagname(c, "Name"))
            repeat = escape(get_content_by_tagname(c, "Rpt"))
            desc = escape(get_content_by_tagname(c, "Description"))
            l = get_content_by_tagname(c, "Length")
            usage = escape(get_content_by_tagname(c, "Usage"))
            
            print(f"\t{id} {pos}")
            sql = f"""
                INSERT INTO "Field"
                (id, pos, type, name, description, "table", usage, version, length, repeat)
                VALUES
                ('{id}', {pos}, {t}, {name}, {desc}, {tbl}, {usage}, '{version}', {l}, {repeat})
            """
            con.execute(sql)
        con.commit()

def importTables():
    global ns
    file = f"data/xml/{version}/Tables.xml"
    ns = get_ns(file)
    #print(ns)

    # open type list
    tree = ET.parse(file)
    root = tree.getroot()
    typeList = get_node_by_tagname(root, "ElementSummaryEnvelope")

    # parse datatypes
    for e in typeList:
        id = get_node_by_tagname(e, "Id")[0].text
        name = escape(get_content_by_tagname(e, "Label"))
        desc = escape(get_content_by_tagname(e, "Description"))
        chapt = ""
        try:
            chapt = get_node_by_tagname(e, "string", "d3p1")[0].text
        except: pass

        # remove id from name
        name = re.sub(id + " ?- *", "", name)

        print(id + " - " + name)

        # parse details and lists
        file2 = f"data/xml/{version}/Tables/{id}.xml"
        tree = ET.parse(file2).getroot()

        children = get_node_by_tagname(tree, "TableEntryEnvelope")
        tp = get_node_by_tagname(tree, "TableType")[0].text

        # skip preloaded tables
        if tp == "PreLoaded":
            continue

        sql = f"""
            INSERT INTO "Tables"
            (id, name, description, childCount, version, chapter, "type")
            VALUES
            ('{id}', {name}, {desc}, {len(children)}, '{version}', {escape(chapt)}, '{tp}')
        """
        #print(sql)
        con.execute(sql)
        con.commit()

        # loop over all children and import them
        for c in children:
            v = escape(get_content_by_tagname(c, "Value"))
            desc = escape(get_content_by_tagname(c, "Description"))
            
            print(f"\t{id} {v}")
            sql = f"""
                INSERT INTO "TableEntries"
                (id, value, description, version)
                VALUES
                ('{id}', {v}, {desc}, '{version}')
            """
            con.execute(sql)
        con.commit()

def main(argv):
    importDataTypes()
    importTables()
    importSegments()

if __name__ == "__main__":
    con, cur = dbConnection(dbfile)
    version = sys.argv[1]

    vid = int(version.replace(".", ""))
    if vid < 100: vid = vid*10
    sql = f"insert into Version (id, version) VALUES ({vid}, '{version}')"
    con.execute(sql)
    con.commit()

    main(sys.argv)